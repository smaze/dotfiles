font pango:monospace 1

default_border pixel 2
smart_borders on

# Logo key. Use Mod1 for Alt.
set $mod Mod4
# Home row direction keys, like vim
set $left h
set $down j
set $up k
set $right l

set $term foot
for_window [app_id="^launcher$"] floating enable, sticky enable, resize set 30 ppt 60 ppt, border pixel 5
set $menu exec $term --app-id=launcher -e sway-launcher-desktop
set $menu exec j4-dmenu-desktop --dmenu="bemenu -in"

#set $wallpaper /home/smaze/Pictures/current-wallpaper.jpg
set $wallpaper /home/smaze/Downloads/ffc72f68956835.5b70b6be8edc5.jpg
#exec oguri

for_window [app_id="mpv"] floating enable
for_window [app_id="imv"] floating enable
for_window [title=".+[Ss]haring (Indicator|your screen)"] floating enable, move to scratchpad

output * bg $wallpaper fill
output * render_bit_depth 10
output * adaptive_sync on
#output DP-1 mode 2560x1440@120Hz
#output eDP-1 max_render_time 9
output DP-1 max_render_time 7

#output eDP-1 subpixel rgb
#output eDP-1 disable
set $laptop eDP-1
bindswitch --reload --locked lid:on output $laptop disable
bindswitch --reload --locked lid:off output $laptop enable


### Idle configuration

exec swayidle -w \
         timeout 60 "makoctl set-mode away" resume "makoctl set-mode default" \
         timeout 300 'swaylock -f -i $wallpaper -s fill -u' timeout 600 'swaymsg output DP-1 power off' resume 'swaymsg "output DP-1 power on"' \
         before-sleep 'swaylock -f -i $wallpaper -s fill'

### Input configuration

input type:touchpad {
       accel_profile flat
       pointer_accel 0
       dwt disabled
       tap enabled
       natural_scroll enabled
       middle_emulation enabled
   }

input type:mouse {
    accel_profile flat
    pointer_accel 0
}

input type:keyboard {
    xkb_options compose:ralt
    xkb_numlock enabled
}

input  "1267:10026:ELAN0732:00_04F3:272A" {
     events disabled
     map_to_output eDP-1
}

# You can get the names of your inputs by running: swaymsg -t get_inputs
# Read `man 5 sway-input` for more information about this section.

### Key bindings
#
# Basics:
#
    # Start a terminal
    bindsym $mod+Return exec $term

    # Kill focused window
    bindsym $mod+Shift+q kill

    # Start your launcher
    bindsym $mod+d exec $menu

    # Drag floating windows by holding down $mod and left mouse button.
    # Resize them with right mouse button + $mod.
    # Despite the name, also works for non-floating windows.
    # Change normal to inverse to use left mouse button for resizing and right
    # mouse button for dragging.
    floating_modifier $mod normal

    # Reload the configuration file
    bindsym $mod+Shift+c reload

    # Exit sway (logs you out of your Wayland session)
    bindsym $mod+Shift+e exec swaynag -t warning -m 'You pressed the exit shortcut. Do you really want to exit sway? This will end your Wayland session.' -B 'Yes, exit sway' 'swaymsg exit'
#
# Moving around:
#
    # Move your focus around
    bindsym $mod+$left focus left
    bindsym $mod+$down focus down
    bindsym $mod+$up focus up
    bindsym $mod+$right focus right
    # Or use $mod+[up|down|left|right]
    bindsym $mod+Left focus left
    bindsym $mod+Down focus down
    bindsym $mod+Up focus up
    bindsym $mod+Right focus right

    # Move the focused window with the same, but add Shift
    bindsym $mod+Shift+$left move left
    bindsym $mod+Shift+$down move down
    bindsym $mod+Shift+$up move up
    bindsym $mod+Shift+$right move right
    # Ditto, with arrow keys
    bindsym $mod+Shift+Left move left
    bindsym $mod+Shift+Down move down
    bindsym $mod+Shift+Up move up
    bindsym $mod+Shift+Right move right
#
# Workspaces:
#
    # Switch to workspace
    bindsym --no-repeat $mod+1 workspace number 1
    bindsym --no-repeat $mod+2 workspace number 2
    bindsym --no-repeat $mod+3 workspace number 3
    bindsym --no-repeat $mod+4 workspace number 4
    bindsym --no-repeat $mod+5 workspace number 5
    bindsym --no-repeat $mod+6 workspace number 6
    bindsym --no-repeat $mod+7 workspace number 7
    bindsym --no-repeat $mod+8 workspace number 8
    bindsym --no-repeat $mod+9 workspace number 9
    bindsym --no-repeat $mod+0 workspace number 10

#    bindsym --release $mod+1 exec "echo 0 > /tmp/sovpipe"
#    bindsym --release $mod+2 exec "echo 0 > /tmp/sovpipe"
#    bindsym --release $mod+3 exec "echo 0 > /tmp/sovpipe"
#    bindsym --release $mod+4 exec "echo 0 > /tmp/sovpipe"
#    bindsym --release $mod+5 exec "echo 0 > /tmp/sovpipe"
#    bindsym --release $mod+6 exec "echo 0 > /tmp/sovpipe"
#    bindsym --release $mod+7 exec "echo 0 > /tmp/sovpipe"
#    bindsym --release $mod+8 exec "echo 0 > /tmp/sovpipe"
#    bindsym --release $mod+9 exec "echo 0 > /tmp/sovpipe"
#    bindsym --release $mod+0 exec "echo 0 > /tmp/sovpipe"

    # Move focused container to workspace
    bindsym $mod+Shift+1 move container to workspace number 1
    bindsym $mod+Shift+2 move container to workspace number 2
    bindsym $mod+Shift+3 move container to workspace number 3
    bindsym $mod+Shift+4 move container to workspace number 4
    bindsym $mod+Shift+5 move container to workspace number 5
    bindsym $mod+Shift+6 move container to workspace number 6
    bindsym $mod+Shift+7 move container to workspace number 7
    bindsym $mod+Shift+8 move container to workspace number 8
    bindsym $mod+Shift+9 move container to workspace number 9
    bindsym $mod+Shift+0 move container to workspace number 10
    # Note: workspaces can have any name you want, not just numbers.
    # We just use 1-10 as the default.
#
# Layout stuff:
#
    # You can "split" the current object of your focus with
    # $mod+b or $mod+v, for horizontal and vertical splits
    # respectively.
    bindsym $mod+b splith
    bindsym $mod+v splitv

    # Switch the current container between different layout styles
    bindsym $mod+s layout stacking
    bindsym $mod+w layout tabbed
    bindsym $mod+e layout toggle split

    # Make the current focus fullscreen
    bindsym $mod+f fullscreen

    # Toggle the current focus between tiling and floating mode
    bindsym $mod+Shift+space floating toggle

    # Swap focus between the tiling area and the floating area
    #bindsym $mod+Control+space focus mode_toggle

    # Move focus to the parent container
    bindsym $mod+a focus parent
#
# Scratchpad:
#
    # Sway has a "scratchpad", which is a bag of holding for windows.
    # You can send windows there and get them back later.

    # Move the currently focused window to the scratchpad
    bindsym $mod+Shift+minus move scratchpad

    # Show the next scratchpad window or hide the focused scratchpad window.
    # If there are multiple scratchpad windows, this command cycles through them.
    bindsym $mod+minus scratchpad show
#
# Resizing containers:
#
mode "resize" {
    # left will shrink the containers width
    # right will grow the containers width
    # up will shrink the containers height
    # down will grow the containers height
    bindsym $left resize shrink width 10px
    bindsym $down resize grow height 10px
    bindsym $up resize shrink height 10px
    bindsym $right resize grow width 10px

    # Ditto, with arrow keys
    bindsym Left resize shrink width 10px
    bindsym Down resize grow height 10px
    bindsym Up resize shrink height 10px
    bindsym Right resize grow width 10px

    # Return to default mode
    bindsym Return mode "default"
    bindsym Escape mode "default"
}
bindsym $mod+r mode "resize"

bindsym --locked XF86MonBrightnessUp exec brightnessctl -e set +3%
bindsym --locked XF86MonBrightnessDown exec brightnessctl -e set 3%-

bindsym --locked $mod+XF86MonBrightnessUp exec brightnessctl set 100%

bindsym --locked $mod+XF86MonBrightnessDown exec brightnessctl -e set 25%- 


bindsym XF86AudioNext exec playerctl next
bindsym XF86AudioPrev exec playerctl previous
bindsym XF86AudioPlay exec playerctl play-pause
#bindsym --locked Shift+XF86AudioLowerVolume exec playerctl play-pause
#bindsym --locked Shift+XF86AudioRaiseVolume exec playerctl next
#bindsym --locked Shift+XF86AudioMute exec playerctl previous

bindsym --locked XF86AudioLowerVolume exec pactl set-sink-volume @DEFAULT_SINK@ -2%
bindsym --locked XF86AudioRaiseVolume exec pactl set-sink-volume @DEFAULT_SINK@ +2%
bindsym --locked XF86AudioMute exec pactl set-sink-mute @DEFAULT_SINK@ toggle

bindsym $mod+Ctrl+space exec --no-startup-id pactl set-source-mute @DEFAULT_SOURCE@ 0 # mute mic
bindsym --release $mod+Ctrl+space exec --no-startup-id pactl set-source-mute @DEFAULT_SOURCE@ 1 # unmute mic

bindsym Print exec grimshot --notify --cursor copy area
bindsym Shift+Print exec grimshot --notify --cursor copy window
bindsym $mod+Print exec 'grim -g "$(slurp)" - | tesseract stdin stdout -l eng | wl-copy'

bindsym $mod+Ctrl+l exec swaylock -f -i $wallpaper -s fill -u

bindsym $mod+period exec wofi-emoji

bindsym $mod+p exec /home/smaze/autocircle.sh

bindsym $mod+Escape exec makoctl dismiss

bindsym $mod+Shift+a exec notify-send $(date)

#bar {
#    position top
#
#    #status_command while date +'%Y-%m-%d %l:%M:%S %p'; do sleep 1; done
#    status_command ~/.config/sway/statusbar
#    colors {
#        statusline #ffffff
#        background #323232
#        inactive_workspace #32323200 #32323200 #5c5c5c
#    }
#}

focus_wrapping no

exec_always bash -c "pkill kanshi; kanshi"
exec waybar
exec wlsunset -l 43.683334 -L -79.766670
exec libinput-gestures
#exec rot8
#exec rm -f /tmp/sovpipe && mkfifo /tmp/sovpipe && tail -f /tmp/sovpipe | sov
exec polkit-dumb-agent

set $gnome-schema org.gnome.desktop.interface

exec_always {
    gsettings set $gnome-schema gtk-theme 'rose-pine-gtk'
    gsettings set $gnome-schema font-name 'IBM Plex Sans 11'
}

exec systemctl --user import-environment WAYLAND_DISPLAY XDG_CURRENT_DESKTOP

include /etc/sway/config.d/*
